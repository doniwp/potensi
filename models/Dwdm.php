<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_dwdm".
 *
 * @property string $id_dwdm
 * @property string $nama_dwdm
 * @property string $alias_dwdm
 *
 * @property DwdmPort[] $mstDwdmPorts
 */
class Dwdm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_dwdm';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_dwdm', 'alias_dwdm'], 'required'],
            [['nama_dwdm'], 'string', 'max' => 50],
            [['alias_dwdm'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_dwdm' => 'Id Dwdm',
            'nama_dwdm' => 'Nama DWDM',
            'alias_dwdm' => 'Alias DWDM',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDwdmPorts()
    {
        return $this->hasMany(DwdmPort::className(), ['id_dwdm' => 'id_dwdm']);
    }
}
