<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_dwdm_port".
 *
 * @property string $id_dport
 * @property string $id_dwdm
 * @property string $id_port1
 * @property string $id_port2
 * @property string $id_port3
 * @property string $status
 *
 * @property Dwdm $dwdm
 * @property Port $port1
 * @property Port $port2
 * @property Port $port3
 */
class DwdmPort extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_dwdm_port';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_dwdm', 'id_port1', 'id_port2', 'id_port3'], 'required'],
            [['id_dwdm', 'id_port1', 'id_port2', 'id_port3'], 'integer'],
            [['status'], 'string'],
            [['id_dwdm'], 'exist', 'skipOnError' => true, 'targetClass' => Dwdm::className(), 'targetAttribute' => ['id_dwdm' => 'id_dwdm']],
            [['id_port1'], 'exist', 'skipOnError' => true, 'targetClass' => Port::className(), 'targetAttribute' => ['id_port1' => 'id_port']],
            [['id_port2'], 'exist', 'skipOnError' => true, 'targetClass' => Port::className(), 'targetAttribute' => ['id_port2' => 'id_port']],
            [['id_port3'], 'exist', 'skipOnError' => true, 'targetClass' => Port::className(), 'targetAttribute' => ['id_port3' => 'id_port']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_dport' => 'Id Dport',
            'id_dwdm' => 'Id Dwdm',
            'id_port1' => 'Port-1',
            'id_port2' => 'Port-2',
            'id_port3' => 'Port-3',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDwdm()
    {
        return $this->hasOne(Dwdm::className(), ['id_dwdm' => 'id_dwdm']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort1()
    {
        return $this->hasOne(Port::className(), ['id_port' => 'id_port1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort2()
    {
        return $this->hasOne(Port::className(), ['id_port' => 'id_port2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort3()
    {
        return $this->hasOne(Port::className(), ['id_port' => 'id_port3']);
    }
}
