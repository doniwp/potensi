<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_kapasitas".
 *
 * @property string $id_kapasitas
 * @property string $jml_kapasitas
 *
 * @property TrnPotensiDwdm[] $trnPotensiDwdms
 */
class Kapasitas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_kapasitas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jml_kapasitas'], 'required'],
            [['jml_kapasitas'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_kapasitas' => 'Id Kapasitas',
            'jml_kapasitas' => 'Jml Kapasitas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_kapasitas' => 'id_kapasitas']);
    }
}
