<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_lambda".
 *
 * @property string $id_lambda
 * @property string $no_lambda
 *
 * @property TrnPotensiDwdm[] $trnPotensiDwdms
 */
class Lambda extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_lambda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_lambda'], 'required'],
            [['no_lambda'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_lambda' => 'Id Lambda',
            'no_lambda' => 'No Lambda',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_lambda' => 'id_lambda']);
    }
}
