<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_metro".
 *
 * @property string $id_metro
 * @property string $nama_metro
 * @property string $alias_metro
 *
 * @property MstMetroPort[] $mstMetroPorts
 */
class Metro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_metro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_metro', 'alias_metro'], 'required'],
            [['nama_metro', 'alias_metro'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_metro' => 'Id Metro',
            'nama_metro' => 'Nama Metro',
            'alias_metro' => 'Alias Metro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetroPorts()
    {
        return $this->hasMany(MetroPort::className(), ['id_metro' => 'id_metro']);
    }
}
