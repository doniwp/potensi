<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_metro_port".
 *
 * @property string $id_mport
 * @property string $id_metro
 * @property string $id_port1
 * @property string $id_port2
 * @property string $id_port3
 * @property string $status
 *
 * @property Metro $metro
 * @property Port $port1
 * @property Port $port2
 * @property Port $port3
 */
class MetroPort extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_metro_port';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_metro', 'id_port1', 'id_port2', 'id_port3'], 'required'],
            [['id_metro', 'id_port1', 'id_port2', 'id_port3'], 'integer'],
            [['status'], 'string'],
            [['id_metro'], 'exist', 'skipOnError' => true, 'targetClass' => Metro::className(), 'targetAttribute' => ['id_metro' => 'id_metro']],
            [['id_port1'], 'exist', 'skipOnError' => true, 'targetClass' => Port::className(), 'targetAttribute' => ['id_port1' => 'id_port']],
            [['id_port2'], 'exist', 'skipOnError' => true, 'targetClass' => Port::className(), 'targetAttribute' => ['id_port2' => 'id_port']],
            [['id_port3'], 'exist', 'skipOnError' => true, 'targetClass' => Port::className(), 'targetAttribute' => ['id_port3' => 'id_port']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_mport' => 'Id Mport',
            'id_metro' => 'Id Metro',
            'id_port1' => 'Port-1',
            'id_port2' => 'Port-2',
            'id_port3' => 'Port-3',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetro()
    {
        return $this->hasOne(Metro::className(), ['id_metro' => 'id_metro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort1()
    {
        return $this->hasOne(Port::className(), ['id_port' => 'id_port1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort2()
    {
        return $this->hasOne(Port::className(), ['id_port' => 'id_port2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort3()
    {
        return $this->hasOne(Port::className(), ['id_port' => 'id_port3']);
    }
}
