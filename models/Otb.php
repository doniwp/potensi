<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_otb".
 *
 * @property string $id_otb
 * @property string $nama_otb
 * @property string $alias_otb
 *
 * @property OtbPort[] $mstOtbPorts
 */
class Otb extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_otb';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_otb', 'alias_otb'], 'required'],
            [['nama_otb'], 'string', 'max' => 50],
            [['alias_otb'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_otb' => 'Id Otb',
            'nama_otb' => 'Nama OTB',
            'alias_otb' => 'Alias OTB',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOtbPorts()
    {
        return $this->hasMany(OtbPort::className(), ['id_otb' => 'id_otb']);
    }
}
