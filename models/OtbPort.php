<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_otb_port".
 *
 * @property string $id_oport
 * @property string $id_otb
 * @property string $id_port1
 * @property string $id_port2
 * @property string $id_port3
 * @property string $id_port4
 * @property string $id_port5
 * @property string $status
 *
 * @property Otb $otb
 * @property Port $port1
 * @property Port $port2
 * @property Port $port3
 * @property Port $port4
 * @property Port $port5
 */
class OtbPort extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_otb_port';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_otb', 'id_port1', 'id_port2', 'id_port3', 'id_port4', 'id_port5'], 'required'],
            [['id_otb', 'id_port1', 'id_port2', 'id_port3', 'id_port4', 'id_port5'], 'integer'],
            [['status'], 'string'],
            [['id_otb'], 'exist', 'skipOnError' => true, 'targetClass' => Otb::className(), 'targetAttribute' => ['id_otb' => 'id_otb']],
            [['id_port1'], 'exist', 'skipOnError' => true, 'targetClass' => Port::className(), 'targetAttribute' => ['id_port1' => 'id_port']],
            [['id_port2'], 'exist', 'skipOnError' => true, 'targetClass' => Port::className(), 'targetAttribute' => ['id_port2' => 'id_port']],
            [['id_port3'], 'exist', 'skipOnError' => true, 'targetClass' => Port::className(), 'targetAttribute' => ['id_port3' => 'id_port']],
            [['id_port4'], 'exist', 'skipOnError' => true, 'targetClass' => Port::className(), 'targetAttribute' => ['id_port4' => 'id_port']],
            [['id_port5'], 'exist', 'skipOnError' => true, 'targetClass' => Port::className(), 'targetAttribute' => ['id_port5' => 'id_port']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_oport' => 'Id Oport',
            'id_otb' => 'Id Otb',
            'id_port1' => 'Port-1',
            'id_port2' => 'Port-2',
            'id_port3' => 'Port-3',
            'id_port4' => 'Port-4',
            'id_port5' => 'Port-5',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOtb()
    {
        return $this->hasOne(Otb::className(), ['id_otb' => 'id_otb']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort1()
    {
        return $this->hasOne(Port::className(), ['id_port' => 'id_port1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort2()
    {
        return $this->hasOne(Port::className(), ['id_port' => 'id_port2']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort3()
    {
        return $this->hasOne(Port::className(), ['id_port' => 'id_port3']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort4()
    {
        return $this->hasOne(Port::className(), ['id_port' => 'id_port4']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPort5()
    {
        return $this->hasOne(Port::className(), ['id_port' => 'id_port5']);
    }
}
