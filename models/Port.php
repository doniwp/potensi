<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_port".
 *
 * @property string $id_port
 * @property string $nama_port
 * @property string $tipe_port
 *
 * @property MstDwdmPort[] $mstDwdmPorts
 * @property MstDwdmPort[] $mstDwdmPorts0
 * @property MstDwdmPort[] $mstDwdmPorts1
 * @property MstMetroPort[] $mstMetroPorts
 * @property MstMetroPort[] $mstMetroPorts0
 * @property MstMetroPort[] $mstMetroPorts1
 * @property MstOtbPort[] $mstOtbPorts
 * @property MstOtbPort[] $mstOtbPorts0
 * @property MstOtbPort[] $mstOtbPorts1
 * @property MstOtbPort[] $mstOtbPorts2
 * @property MstOtbPort[] $mstOtbPorts3
 * @property TrnPotensiDwdm[] $trnPotensiDwdms
 * @property TrnPotensiDwdm[] $trnPotensiDwdms0
 * @property TrnPotensiDwdm[] $trnPotensiDwdms1
 * @property TrnPotensiDwdm[] $trnPotensiDwdms2
 * @property TrnPotensiDwdm[] $trnPotensiDwdms3
 * @property TrnPotensiDwdm[] $trnPotensiDwdms4
 * @property TrnPotensiDwdm[] $trnPotensiDwdms5
 * @property TrnPotensiDwdm[] $trnPotensiDwdms6
 * @property TrnPotensiDwdm[] $trnPotensiDwdms7
 * @property TrnPotensiDwdm[] $trnPotensiDwdms8
 * @property TrnPotensiDwdm[] $trnPotensiDwdms9
 * @property TrnPotensiDwdm[] $trnPotensiDwdms10
 */
class Port extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_port';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_port'], 'required'],
            [['tipe_port'], 'string'],
            [['nama_port'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_port' => 'Id Port',
            'nama_port' => 'Nama Port',
            'tipe_port' => 'Tipe Port',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstDwdmPorts()
    {
        return $this->hasMany(MstDwdmPort::className(), ['id_port1' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstDwdmPorts0()
    {
        return $this->hasMany(MstDwdmPort::className(), ['id_port2' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstDwdmPorts1()
    {
        return $this->hasMany(MstDwdmPort::className(), ['id_port3' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstMetroPorts()
    {
        return $this->hasMany(MstMetroPort::className(), ['id_port1' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstMetroPorts0()
    {
        return $this->hasMany(MstMetroPort::className(), ['id_port2' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstMetroPorts1()
    {
        return $this->hasMany(MstMetroPort::className(), ['id_port3' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstOtbPorts()
    {
        return $this->hasMany(MstOtbPort::className(), ['id_port1' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstOtbPorts0()
    {
        return $this->hasMany(MstOtbPort::className(), ['id_port2' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstOtbPorts1()
    {
        return $this->hasMany(MstOtbPort::className(), ['id_port3' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstOtbPorts2()
    {
        return $this->hasMany(MstOtbPort::className(), ['id_port4' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMstOtbPorts3()
    {
        return $this->hasMany(MstOtbPort::className(), ['id_port5' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_port_a1' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms0()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_dwdm_b1' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms1()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_dwdm_b2' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms2()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_dwdm_b3' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms3()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_port_a2' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms4()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_port_a3' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms5()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_port_b1' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms6()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_port_b2' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms7()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_port_b3' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms8()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_dwdm_a1' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms9()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_dwdm_a2' => 'id_port']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms10()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_dwdm_a3' => 'id_port']);
    }
}
