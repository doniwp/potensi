<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trn_potensi".
 *
 * @property string $id_trans
 * @property string $id_ruas_near
 * @property string $id_ruas_end
 * @property string $id_mport_a
 * @property string $id_mport_b
 * @property string $id_slot_a
 * @property string $id_slot_b
 * @property string $id_dport_a
 * @property string $id_dport_b
 * @property string $id_oport_a
 * @property string $id_oport_b
 * @property string $id_kapasitas
 * @property string $id_lambda
 *
 * @property Kapasitas $kapasitas
 * @property Lambda $lambda
 * @property Ruas $ruasNear
 * @property Ruas $ruasEnd
 * @property Slot $slotA
 * @property Slot $slotB
 * @property DwdmPort $dportA
 * @property DwdmPort $dportB
 * @property MetroPort $mportA
 * @property MetroPort $mportB
 * @property OtbPort $oportA
 * @property OtbPort $oportB
 * @property PotensiPath[] $trnPotensiPaths
 */
class Potensi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trn_potensi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ruas_near', 'id_ruas_end', 'id_mport_a'], 'required'],
            [['id_ruas_near', 'id_ruas_end', 'id_mport_a', 'id_mport_b', 'id_slot_a', 'id_slot_b', 'id_dport_a', 'id_dport_b', 'id_oport_a', 'id_oport_b', 'id_kapasitas', 'id_lambda'], 'integer'],
            [['id_kapasitas'], 'exist', 'skipOnError' => true, 'targetClass' => Kapasitas::className(), 'targetAttribute' => ['id_kapasitas' => 'id_kapasitas']],
            [['id_lambda'], 'exist', 'skipOnError' => true, 'targetClass' => Lambda::className(), 'targetAttribute' => ['id_lambda' => 'id_lambda']],
            [['id_ruas_near'], 'exist', 'skipOnError' => true, 'targetClass' => Ruas::className(), 'targetAttribute' => ['id_ruas_near' => 'id_ruas']],
            [['id_ruas_end'], 'exist', 'skipOnError' => true, 'targetClass' => Ruas::className(), 'targetAttribute' => ['id_ruas_end' => 'id_ruas']],
            [['id_slot_a'], 'exist', 'skipOnError' => true, 'targetClass' => Slot::className(), 'targetAttribute' => ['id_slot_a' => 'id_slot']],
            [['id_slot_b'], 'exist', 'skipOnError' => true, 'targetClass' => Slot::className(), 'targetAttribute' => ['id_slot_b' => 'id_slot']],
            [['id_dport_a'], 'exist', 'skipOnError' => true, 'targetClass' => DwdmPort::className(), 'targetAttribute' => ['id_dport_a' => 'id_dport']],
            [['id_dport_b'], 'exist', 'skipOnError' => true, 'targetClass' => DwdmPort::className(), 'targetAttribute' => ['id_dport_b' => 'id_dport']],
            [['id_mport_a'], 'exist', 'skipOnError' => true, 'targetClass' => MetroPort::className(), 'targetAttribute' => ['id_mport_a' => 'id_mport']],
            [['id_mport_b'], 'exist', 'skipOnError' => true, 'targetClass' => MetroPort::className(), 'targetAttribute' => ['id_mport_b' => 'id_mport']],
            [['id_oport_a'], 'exist', 'skipOnError' => true, 'targetClass' => OtbPort::className(), 'targetAttribute' => ['id_oport_a' => 'id_oport']],
            [['id_oport_b'], 'exist', 'skipOnError' => true, 'targetClass' => OtbPort::className(), 'targetAttribute' => ['id_oport_b' => 'id_oport']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_trans' => 'Id Trans',
            'id_ruas_near' => 'Site-A (Near)',
            'id_ruas_end' => 'Site-B (End)',
            'id_mport_a' => 'Metro A',
            'id_mport_b' => 'Metro B',
            'id_slot_a' => 'Slot A',
            'id_slot_b' => 'Slot B',
            'id_dport_a' => 'DWDM A',
            'id_dport_b' => 'DWDM B',
            'id_oport_a' => 'OTB A',
            'id_oport_b' => 'OTB B',
            'id_kapasitas' => 'Kapasitas',
            'id_lambda' => 'Lambda',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKapasitas()
    {
        return $this->hasOne(Kapasitas::className(), ['id_kapasitas' => 'id_kapasitas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLambda()
    {
        return $this->hasOne(Lambda::className(), ['id_lambda' => 'id_lambda']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuasNear()
    {
        return $this->hasOne(Ruas::className(), ['id_ruas' => 'id_ruas_near']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuasEnd()
    {
        return $this->hasOne(Ruas::className(), ['id_ruas' => 'id_ruas_end']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlotA()
    {
        return $this->hasOne(Slot::className(), ['id_slot' => 'id_slot_a']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlotB()
    {
        return $this->hasOne(Slot::className(), ['id_slot' => 'id_slot_b']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDportA()
    {
        return $this->hasOne(DwdmPort::className(), ['id_dport' => 'id_dport_a']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDportB()
    {
        return $this->hasOne(DwdmPort::className(), ['id_dport' => 'id_dport_b']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMportA()
    {
        return $this->hasOne(MetroPort::className(), ['id_mport' => 'id_mport_a']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMportB()
    {
        return $this->hasOne(MetroPort::className(), ['id_mport' => 'id_mport_b']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportA()
    {
        return $this->hasOne(OtbPort::className(), ['id_oport' => 'id_oport_a']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportB()
    {
        return $this->hasOne(OtbPort::className(), ['id_oport' => 'id_oport_b']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPotensiPaths()
    {
        return $this->hasMany(PotensiPath::className(), ['id_trans' => 'id_trans']);
    }
}
