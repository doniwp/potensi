<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trn_potensi_path".
 *
 * @property string $id_path
 * @property string $id_trans
 * @property string $id_ruas
 *
 * @property Ruas $ruas
 * @property Potensi $trans
 */
class PotensiPath extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trn_potensi_path';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_trans', 'id_ruas'], 'required'],
            [['id_trans', 'id_ruas'], 'integer'],
            [['id_ruas'], 'exist', 'skipOnError' => true, 'targetClass' => Ruas::className(), 'targetAttribute' => ['id_ruas' => 'id_ruas']],
            [['id_trans'], 'exist', 'skipOnError' => true, 'targetClass' => Potensi::className(), 'targetAttribute' => ['id_trans' => 'id_trans']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_path' => 'Id Path',
            'id_trans' => 'Id Trans',
            'id_ruas' => 'Id Ruas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuas()
    {
        return $this->hasOne(Ruas::className(), ['id_ruas' => 'id_ruas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrans()
    {
        return $this->hasOne(Potensi::className(), ['id_trans' => 'id_trans']);
    }
}
