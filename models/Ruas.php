<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_ruas".
 *
 * @property string $id_ruas
 * @property string $nama_ruas
 * @property string $alias_ruas
 *
 * @property TrnDwdmPath[] $trnDwdmPaths
 * @property TrnPotensiDwdm[] $trnPotensiDwdms
 * @property TrnPotensiDwdm[] $trnPotensiDwdms0
 */
class Ruas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_ruas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_ruas', 'alias_ruas'], 'required'],
            [['nama_ruas'], 'string', 'max' => 50],
            [['alias_ruas'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_ruas' => 'Id Ruas',
            'nama_ruas' => 'Nama Ruas',
            'alias_ruas' => 'Alias Ruas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnDwdmPaths()
    {
        return $this->hasMany(TrnDwdmPath::className(), ['id_ruas' => 'id_ruas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_ruas_near' => 'id_ruas']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms0()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_ruas_end' => 'id_ruas']);
    }
}
