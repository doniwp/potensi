<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mst_slot".
 *
 * @property string $id_slot
 * @property string $nama_slot
 *
 * @property TrnPotensiDwdm[] $trnPotensiDwdms
 * @property TrnPotensiDwdm[] $trnPotensiDwdms0
 */
class Slot extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mst_slot';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_slot'], 'required'],
            [['nama_slot'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_slot' => 'Id Slot',
            'nama_slot' => 'Nama Slot',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_slot_a' => 'id_slot']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrnPotensiDwdms0()
    {
        return $this->hasMany(TrnPotensiDwdm::className(), ['id_slot_b' => 'id_slot']);
    }
}
