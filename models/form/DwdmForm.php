<?php
/**
 * Created by PhpStorm.
 * User: Doni Wahyu Prasetyo
 * Date: 12/3/2018
 * Time: 1:50 PM
 */

namespace app\models\form;


use yii\base\Model;
use app\models\Dwdm;
use app\models\DwdmPort;
use Yii;

class DwdmForm extends Model
{

    private $_dwdm;
    private $_dwdmports;

    public function rules()
    {
        return [
            [['Dwdm'], 'required'],
            [['Dwdmports'], 'safe'],
        ];
    }

    public function afterValidate()
    {
        if (!Model::validateMultiple($this->getAllModels())) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    public function save()
    {
//        if (!$this->validate()) {
//            return false;
//        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->dwdm->save()) {
            $transaction->rollBack();
            return false;
        }
        if (!$this->saveDwdmports()) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        return true;
    }

    public function saveDwdmports()
    {
        $keep = [];
        foreach ($this->dwdmports as $dwdmport) {
            $dwdmport->id_dwdm = $this->dwdm->id_dwdm;
            if (!$dwdmport->save(false)) {
                return false;
            }
            $keep[] = $dwdmport->id_dport;
        }
        $query = DwdmPort::find()->andWhere(['id_dwdm' => $this->dwdm->id_dwdm]);
        if ($keep) {
            $query->andWhere(['not in', 'id_dport', $keep]);
        }
        foreach ($query->all() as $dwdmport) {
            $dwdmport->delete();
        }
        return true;
    }

    public function getDwdm()
    {
        return $this->_dwdm;
    }

    public function setDwdm($dwdm)
    {
        if ($dwdm instanceof Dwdm) {
            $this->_dwdm = $dwdm;
        } else if (is_array($dwdm)) {
            $this->_dwdm->setAttributes($dwdm);
        }
    }

    public function getDwdmports()
    {
        if ($this->_dwdmports === null) {
            $this->_dwdmports = $this->dwdm->isNewRecord ? [] : $this->dwdm->dwdmPorts;
        }
        return $this->_dwdmports;
    }

    private function getDwdmport($key)
    {
        $dwdmport = $key && strpos($key, 'new') === false ? Dwdmport::findOne($key) : false;
        if (!$dwdmport) {
            $dwdmport = new DwdmPort();
            $dwdmport->loadDefaultValues();
        }
        return $dwdmport;
    }

    public function setDwdmports($dwdmports)
    {
        unset($dwdmports['__id__']); // remove the hidden "new Dwdmport" row
        $this->_dwdmports = [];
        foreach ($dwdmports as $key => $dwdmport) {
            if (is_array($dwdmport)) {
                $this->_dwdmports[$key] = $this->getDwdmport($key);
                $this->_dwdmports[$key]->setAttributes($dwdmport);
            } elseif ($dwdmport instanceof DwdmPort) {
                $this->_dwdmports[$dwdmport->id_dport] = $dwdmport;
            }
        }
    }

    public function errorSummary($form)
    {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }

    private function getAllModels()
    {
        $models = [
            'Dwdm' => $this->dwdm,
        ];
        foreach ($this->dwdmports as $id => $dwdmport) {
            $models['Dwdmport.' . $id] = $this->dwdmports[$id];
        }
        return $models;
    }
}