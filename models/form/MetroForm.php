<?php

namespace app\models\form;

use app\models\Metro;
use app\models\MetroPort;
use Yii;
use yii\base\Model;
use yii\widgets\ActiveForm;

/**
 * Created by PhpStorm.
 * User: Doni Wahyu Prasetyo
 * Date: 11/30/2018
 * Time: 8:22 AM
 */
class MetroForm extends Model
{
    private $_metro;
    private $_metroports;

    public function rules()
    {
        return [
            [['Metro'], 'required'],
            [['Metroports'], 'safe'],
        ];
    }

    public function afterValidate()
    {
        if (!Model::validateMultiple($this->getAllModels())) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    public function save()
    {
//        if (!$this->validate()) {
//            return false;
//        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->metro->save()) {
            $transaction->rollBack();
            return false;
        }
        if (!$this->saveMetroports()) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        return true;
    }

    public function saveMetroports()
    {
        $keep = [];
        foreach ($this->metroports as $metroport) {
            $metroport->id_metro = $this->metro->id_metro;
            if (!$metroport->save(false)) {
                return false;
            }
            $keep[] = $metroport->id_mport;
        }
        $query = MetroPort::find()->andWhere(['id_metro' => $this->metro->id_metro]);
        if ($keep) {
            $query->andWhere(['not in', 'id_mport', $keep]);
        }
        foreach ($query->all() as $metroport) {
            $metroport->delete();
        }
        return true;
    }

    public function getMetro()
    {
        return $this->_metro;
    }

    public function setMetro($metro)
    {
        if ($metro instanceof Metro) {
            $this->_metro = $metro;
        } else if (is_array($metro)) {
            $this->_metro->setAttributes($metro);
        }
    }

    public function getMetroports()
    {
        if ($this->_metroports === null) {
            $this->_metroports = $this->metro->isNewRecord ? [] : $this->metro->metroPorts;
        }
        return $this->_metroports;
    }

    private function getMetroport($key)
    {
        $metroport = $key && strpos($key, 'new') === false ? Metroport::findOne($key) : false;
        if (!$metroport) {
            $metroport = new MetroPort();
            $metroport->loadDefaultValues();
        }
        return $metroport;
    }

    public function setMetroports($metroports)
    {
        unset($metroports['__id__']); // remove the hidden "new Metroport" row
        $this->_metroports = [];
        foreach ($metroports as $key => $metroport) {
            if (is_array($metroport)) {
                $this->_metroports[$key] = $this->getMetroport($key);
                $this->_metroports[$key]->setAttributes($metroport);
            } elseif ($metroport instanceof MetroPort) {
                $this->_metroports[$metroport->id_mport] = $metroport;
            }
        }
    }

    public function errorSummary($form)
    {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }

    private function getAllModels()
    {
        $models = [
            'Metro' => $this->metro,
        ];
        foreach ($this->metroports as $id => $metroport) {
            $models['Metroport.' . $id] = $this->metroports[$id];
        }
        return $models;
    }
}