<?php
/**
 * Created by PhpStorm.
 * User: Doni Wahyu Prasetyo
 * Date: 12/3/2018
 * Time: 2:43 PM
 */

namespace app\models\form;

use Yii;
use yii\base\Model;
use app\models\Otb;
use app\models\OtbPort;

class OtbForm extends Model
{

    private $_otb;
    private $_otbports;

    public function rules()
    {
        return [
            [['Otb'], 'required'],
            [['Otbports'], 'safe'],
        ];
    }

    public function afterValidate()
    {
        if (!Model::validateMultiple($this->getAllModels())) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    public function save()
    {
//        if (!$this->validate()) {
//            return false;
//        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->otb->save()) {
            $transaction->rollBack();
            return false;
        }
        if (!$this->saveOtbports()) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        return true;
    }

    public function saveOtbports()
    {
        $keep = [];
        foreach ($this->otbports as $otbport) {
            $otbport->id_otb = $this->otb->id_otb;
            if (!$otbport->save(false)) {
                return false;
            }
            $keep[] = $otbport->id_oport;
        }
        $query = OtbPort::find()->andWhere(['id_otb' => $this->otb->id_otb]);
        if ($keep) {
            $query->andWhere(['not in', 'id_oport', $keep]);
        }
        foreach ($query->all() as $otbport) {
            $otbport->delete();
        }
        return true;
    }

    public function getOtb()
    {
        return $this->_otb;
    }

    public function setOtb($otb)
    {
        if ($otb instanceof Otb) {
            $this->_otb = $otb;
        } else if (is_array($otb)) {
            $this->_otb->setAttributes($otb);
        }
    }

    public function getOtbports()
    {
        if ($this->_otbports === null) {
            $this->_otbports = $this->otb->isNewRecord ? [] : $this->otb->otbPorts;
        }
        return $this->_otbports;
    }

    private function getOtbport($key)
    {
        $otbport = $key && strpos($key, 'new') === false ? Otbport::findOne($key) : false;
        if (!$otbport) {
            $otbport = new OtbPort();
            $otbport->loadDefaultValues();
        }
        return $otbport;
    }

    public function setOtbports($otbports)
    {
        unset($otbports['__id__']); // remove the hidden "new Otbport" row
        $this->_otbports = [];
        foreach ($otbports as $key => $otbport) {
            if (is_array($otbport)) {
                $this->_otbports[$key] = $this->getOtbport($key);
                $this->_otbports[$key]->setAttributes($otbport);
            } elseif ($otbport instanceof OtbPort) {
                $this->_otbports[$otbport->id_oport] = $otbport;
            }
        }
    }

    public function errorSummary($form)
    {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }

    private function getAllModels()
    {
        $models = [
            'Otb' => $this->otb,
        ];
        foreach ($this->otbports as $id => $otbport) {
            $models['Otbport.' . $id] = $this->otbports[$id];
        }
        return $models;
    }
}