<?php
/**
 * Created by PhpStorm.
 * User: Doni Wahyu Prasetyo
 * Date: 12/3/2018
 * Time: 1:45 PM
 */

use app\models\Port;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>
<td width="25%">
    <?= $form->field($dwdmport, 'id_port1')->dropDownList(
        ArrayHelper::map(Port::find()->all(), 'id_port', 'nama_port'),
        [
            'id' => "Dwdmports_{$key}_id_port1",
            'name' => "Dwdmports[$key][id_port1]",
            'class' => 'select2',
            'style' => 'width: 100%',
            'prompt' => 'Select Port',
        ])->label(false) ?>
</td>
<td width="25%">
    <?= $form->field($dwdmport, 'id_port2')->dropDownList(
        ArrayHelper::map(Port::find()->all(), 'id_port', 'nama_port'),
        [
            'id' => "Dwdmports_{$key}_id_port2",
            'name' => "Dwdmports[$key][id_port2]",
            'class' => 'select2',
            'style' => 'width: 100%',
            'prompt' => 'Select Port',
        ])->label(false) ?>
</td>
<td width="25%">
    <?= $form->field($dwdmport, 'id_port3')->dropDownList(
        ArrayHelper::map(Port::find()->all(), 'id_port', 'nama_port'),
        [
            'id' => "Dwdmports_{$key}_id_port3",
            'name' => "Dwdmports[$key][id_port3]",
            'class' => 'select2',
            'style' => 'width: 100%',
            'prompt' => 'Select Port',
        ])->label(false) ?>
</td>
<td>
    <?= $form->field($dwdmport, 'status')->textInput([
        'id' => "Dwdmports_{$key}_status",
        'name' => "Dwdmports[$key][status]",
        'disabled' => '',
    ])->label(false) ?>
</td>

<td>
    <?= Html::a('Remove', 'javascript:void(0);', [
        'class' => 'dwdm-remove-dwdmport-button btn btn-danger btn-xs',
    ]) ?>
</td>