<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\DwdmPort;

/* @var $this yii\web\View */
/* @var $model app\models\Dwdm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false, // TODO get this working with client validation
    ]); ?>
    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model->dwdm, 'nama_dwdm')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <?= $form->field($model->dwdm, 'alias_dwdm')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">DWDM Port</h4>
                    <?php
                    // new dwdmport button
                    echo Html::a('New DWDM Port', 'javascript:void(0);', [
                        'id' => 'dwdm-new-dwdmport-button',
                        'class' => 'pull-right btn btn-primary'
                    ])
                    ?>
                </div>
                <div class="box-body">
                    <?php

                    // dwdmport table
                    $dwdmport = new DwdmPort();
                    $dwdmport->loadDefaultValues();
                    echo '<table id="dwdm-dwdmports" class="table table-condensed table-bordered">';
                    echo '<thead>';
                    echo '<tr>';
                    echo '<th>' . $dwdmport->getAttributeLabel('id_port1') . '</th>';
                    echo '<th>' . $dwdmport->getAttributeLabel('id_port2') . '</th>';
                    echo '<th>' . $dwdmport->getAttributeLabel('id_port3') . '</th>';
                    echo '<th>' . $dwdmport->getAttributeLabel('status') . '</th>';
                    echo '<td>&nbsp;</td>';
                    echo '</tr>';
                    echo '</thead>';
                    echo '<tbody>';

                    // existing dwdmports fields
                    foreach ($model->dwdmports as $key => $_dwdmport) {
                        echo '<tr>';
                        echo $this->render('_form-dwdm-dwdmport', [
                            'key' => $_dwdmport->isNewRecord ? (strpos($key, 'new') !== false ? $key : 'new' . $key) : $_dwdmport->id_dport,
                            'form' => $form,
                            'dwdmport' => $_dwdmport,
                        ]);
                        echo '</tr>';
                    }

                    // new dwdmport fields
                    echo '<tr id="dwdm-new-dwdmport-block" style="display: none;">';
                    echo $this->render('_form-dwdm-dwdmport', [
                        'key' => '__id__',
                        'form' => $form,
                        'dwdmport' => $dwdmport,
                    ]);
                    echo '</tr>';
                    echo '</tbody>';
                    echo '</table>';
                    ?>

                    <?php ob_start(); // output buffer the javascript to register later ?>
                    <script>
                        // add dwdmport button
                        var dwdmport_k = <?php echo isset($key) ? str_replace('new', '', $key) : 0; ?>;
                        $('#dwdm-new-dwdmport-button').on('click', function () {
                            dwdmport_k += 1;
                            $('#dwdm-dwdmports').find('tbody')
                                .append('<tr>' + $('#dwdm-new-dwdmport-block').html().replace(/__id__/g, 'new' + dwdmport_k) + '</tr>');

                            setSelect2();
                        });

                        // remove dwdmport button
                        $(document).on('click', '.dwdm-remove-dwdmport-button', function () {
                            $(this).closest('tbody tr').remove();
                        });

                        <?php
                        // OPTIONAL: click add when the form first loads to display the first new row
                        if (!Yii::$app->request->isPost && $model->dwdm->isNewRecord)
                            echo "$('#dwdm-new-dwdmport-button').click();";
                        ?>

                        //Re-Initialize Select2 Elements on each row
                        function setSelect2() {
                            if ($('.select2').data('select2') == undefined && $('.select2').next().hasClass('select2-container')) {
                                $('.select2').next().remove();
                            }

                            $('.select2').select2({
                                placeholder: 'Select Port',
                            });
                        }

                        $(document).ready(function () {
                            //Initialize Select2 Elements
                            $('.select2').select2({
                                placeholder: 'Select Port',
                            });
                        });
                    </script>
                    <?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 text-right">
            <?= Html::submitButton('<i class="fa fa-save"></i>Save', ['class' => 'btn btn-app bg-green']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
