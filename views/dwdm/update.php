<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dwdm */

$this->title = 'Update DWDM: ' . $model->dwdm->alias_dwdm;
$this->params['breadcrumbs'][] = ['label' => 'DWDM', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dwdm->alias_dwdm, 'url' => ['view', 'id' => $model->dwdm->id_dwdm]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-edit"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
