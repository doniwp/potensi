<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kapasitas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'jml_kapasitas')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 text-right">
            <?= Html::submitButton('<i class="fa fa-save"></i>Save', ['class' => 'btn btn-app bg-green']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
