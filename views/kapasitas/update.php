<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kapasitas */

$this->title = 'Update Kapasitas: ' . $model->jml_kapasitas;
$this->params['breadcrumbs'][] = ['label' => 'Kapasitas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jml_kapasitas, 'url' => ['view', 'id' => $model->id_kapasitas]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-edit"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
