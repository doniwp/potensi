<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kapasitas */

$this->title = $model->jml_kapasitas;
$this->params['breadcrumbs'][] = ['label' => 'Kapasitas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-search"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

        <p>
            <?= Html::a('<i class="fa fa-pencil"></i>Update', ['update', 'id' => $model->id_kapasitas], ['class' => 'btn btn-app bg-blue']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>Delete', ['delete', 'id' => $model->id_kapasitas], [
                'class' => 'btn btn-app bg-red-active',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                'jml_kapasitas',
            ],
        ]) ?>

    </div>
</div>
