<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lambda */

$this->title = 'Update Lambda: ' . $model->no_lambda;
$this->params['breadcrumbs'][] = ['label' => 'Lambda', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->no_lambda, 'url' => ['view', 'id' => $model->id_lambda]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-edit"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
