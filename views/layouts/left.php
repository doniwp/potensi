<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::$app->request->baseUrl ?>/img/telkom-indonesia-160.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->name ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Menu Potensi', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Master Data',
                        'icon' => 'database',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Ruas', 'icon' => 'thumb-tack', 'url' => ['/ruas']],
                            ['label' => 'Port', 'icon' => 'external-link', 'url' => ['/port']],
                            ['label' => 'Slot', 'icon' => 'inbox', 'url' => ['/slot']],
                            ['label' => 'Kapasitas', 'icon' => 'battery-full', 'url' => ['/kapasitas']],
                            ['label' => 'Lambda', 'icon' => 'hourglass-half', 'url' => ['/lambda']],
                            ['label' => 'User', 'icon' => 'users', 'url' => ['/user']],
                        ],
                    ],
                    [
                        'label' => 'Potensi',
                        'icon' => 'plug',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Metro', 'icon' => 'building', 'url' => ['/metro']],
                            ['label' => 'DWDM', 'icon' => 'briefcase', 'url' => ['/dwdm']],
                            ['label' => 'OTB', 'icon' => 'road', 'url' => ['/otb']],
                            ['label' => 'Potensi', 'icon' => 'binoculars', 'url' => ['/potensi']],
                        ],
                    ],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                ],
            ]
        ) ?>

    </section>

</aside>
