<?php
/**
 * Created by PhpStorm.
 * User: Doni Wahyu Prasetyo
 * Date: 11/30/2018
 * Time: 8:55 AM
 */

use app\models\Port;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>
<td width="25%">
    <?= $form->field($metroport, 'id_port1')->dropDownList(
        ArrayHelper::map(Port::find()->all(), 'id_port', 'nama_port'),
        [
            'id' => "Metroports_{$key}_id_port1",
            'name' => "Metroports[$key][id_port1]",
            'class' => 'select2',
            'style' => 'width: 100%',
            'prompt' => 'Select Port',
        ])->label(false) ?>
</td>
<td width="25%">
    <?= $form->field($metroport, 'id_port2')->dropDownList(
        ArrayHelper::map(Port::find()->all(), 'id_port', 'nama_port'),
        [
            'id' => "Metroports_{$key}_id_port2",
            'name' => "Metroports[$key][id_port2]",
            'class' => 'select2',
            'style' => 'width: 100%',
            'prompt' => 'Select Port',
        ])->label(false) ?>
</td>
<td width="25%">
    <?= $form->field($metroport, 'id_port3')->dropDownList(
        ArrayHelper::map(Port::find()->all(), 'id_port', 'nama_port'),
        [
            'id' => "Metroports_{$key}_id_port3",
            'name' => "Metroports[$key][id_port3]",
            'class' => 'select2',
            'style' => 'width: 100%',
            'prompt' => 'Select Port',
        ])->label(false) ?>
</td>
<td>
    <?= $form->field($metroport, 'status')->textInput([
        'id' => "Metroports_{$key}_status",
        'name' => "Metroports[$key][status]",
        'disabled' => '',
    ])->label(false) ?>
</td>

<td>
    <?= Html::a('Remove', 'javascript:void(0);', [
        'class' => 'metro-remove-metroport-button btn btn-danger btn-xs',
    ]) ?>
</td>
