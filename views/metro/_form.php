<?php

use app\models\MetroPort;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Metro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false, // TODO get this working with client validation
    ]); ?>
    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model->metro, 'nama_metro')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <?= $form->field($model->metro, 'alias_metro')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">Metro Port</h4>
                    <?php
                    // new metroport button
                    echo Html::a('New Metro Port', 'javascript:void(0);', [
                        'id' => 'metro-new-metroport-button',
                        'class' => 'pull-right btn btn-primary'
                    ])
                    ?>
                </div>
                <div class="box-body">
                    <?php

                    // metroport table
                    $metroport = new MetroPort();
                    $metroport->loadDefaultValues();
                    echo '<table id="metro-metroports" class="table table-condensed table-bordered">';
                    echo '<thead>';
                    echo '<tr>';
                    echo '<th>' . $metroport->getAttributeLabel('id_port1') . '</th>';
                    echo '<th>' . $metroport->getAttributeLabel('id_port2') . '</th>';
                    echo '<th>' . $metroport->getAttributeLabel('id_port3') . '</th>';
                    echo '<th>' . $metroport->getAttributeLabel('status') . '</th>';
                    echo '<td>&nbsp;</td>';
                    echo '</tr>';
                    echo '</thead>';
                    echo '<tbody>';

                    // existing metroports fields
                    foreach ($model->metroports as $key => $_metroport) {
                        echo '<tr>';
                        echo $this->render('_form-metro-metroport', [
                            'key' => $_metroport->isNewRecord ? (strpos($key, 'new') !== false ? $key : 'new' . $key) : $_metroport->id_mport,
                            'form' => $form,
                            'metroport' => $_metroport,
                        ]);
                        echo '</tr>';
                    }

                    // new metroport fields
                    echo '<tr id="metro-new-metroport-block" style="display: none;">';
                    echo $this->render('_form-metro-metroport', [
                        'key' => '__id__',
                        'form' => $form,
                        'metroport' => $metroport,
                    ]);
                    echo '</tr>';
                    echo '</tbody>';
                    echo '</table>';
                    ?>

                    <?php ob_start(); // output buffer the javascript to register later ?>
                    <script>
                        // add metroport button
                        var metroport_k = <?php echo isset($key) ? str_replace('new', '', $key) : 0; ?>;
                        $('#metro-new-metroport-button').on('click', function () {
                            metroport_k += 1;
                            $('#metro-metroports').find('tbody')
                                .append('<tr>' + $('#metro-new-metroport-block').html().replace(/__id__/g, 'new' + metroport_k) + '</tr>');

                            setSelect2();
                        });

                        // remove metroport button
                        $(document).on('click', '.metro-remove-metroport-button', function () {
                            $(this).closest('tbody tr').remove();
                        });

                        <?php
                        // OPTIONAL: click add when the form first loads to display the first new row
                        if (!Yii::$app->request->isPost && $model->metro->isNewRecord)
                            echo "$('#metro-new-metroport-button').click();";
                        ?>

                        //Re-Initialize Select2 Elements on each row
                        function setSelect2() {
                            if ($('.select2').data('select2') == undefined && $('.select2').next().hasClass('select2-container')) {
                                $('.select2').next().remove();
                            }

                            $('.select2').select2({
                                placeholder: 'Select Port',
                            });
                        }

                        $(document).ready(function () {
                            //Initialize Select2 Elements
                            $('.select2').select2({
                                placeholder: 'Select Port',
                            });
                        });
                    </script>
                    <?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 text-right">
            <?= Html::submitButton('<i class="fa fa-save"></i>Save', ['class' => 'btn btn-app bg-green']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

