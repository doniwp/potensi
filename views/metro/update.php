<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Metro */

$this->title = 'Update Metro: ' . $model->metro->alias_metro;
$this->params['breadcrumbs'][] = ['label' => 'Metro', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->metro->alias_metro, 'url' => ['view', 'id' => $model->metro->id_metro]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-edit"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
