<?php
/**
 * Created by PhpStorm.
 * User: Doni Wahyu Prasetyo
 * Date: 12/3/2018
 * Time: 2:40 PM
 */

use app\models\Port;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>
<td width="15%">
    <?= $form->field($otbport, 'id_port1')->dropDownList(
        ArrayHelper::map(Port::find()->all(), 'id_port', 'nama_port'),
        [
            'id' => "Otbports_{$key}_id_port1",
            'name' => "Otbports[$key][id_port1]",
            'class' => 'select2',
            'style' => 'width: 100%',
            'prompt' => 'Select Port',
        ])->label(false) ?>
</td>
<td width="15%">
    <?= $form->field($otbport, 'id_port2')->dropDownList(
        ArrayHelper::map(Port::find()->all(), 'id_port', 'nama_port'),
        [
            'id' => "Otbports_{$key}_id_port2",
            'name' => "Otbports[$key][id_port2]",
            'class' => 'select2',
            'style' => 'width: 100%',
            'prompt' => 'Select Port',
        ])->label(false) ?>
</td>
<td width="15%">
    <?= $form->field($otbport, 'id_port3')->dropDownList(
        ArrayHelper::map(Port::find()->all(), 'id_port', 'nama_port'),
        [
            'id' => "Otbports_{$key}_id_port3",
            'name' => "Otbports[$key][id_port3]",
            'class' => 'select2',
            'style' => 'width: 100%',
            'prompt' => 'Select Port',
        ])->label(false) ?>
</td>
<td width="15%">
    <?= $form->field($otbport, 'id_port4')->dropDownList(
        ArrayHelper::map(Port::find()->all(), 'id_port', 'nama_port'),
        [
            'id' => "Otbports_{$key}_id_port4",
            'name' => "Otbports[$key][id_port4]",
            'class' => 'select2',
            'style' => 'width: 100%',
            'prompt' => 'Select Port',
        ])->label(false) ?>
</td>
<td width="15%">
    <?= $form->field($otbport, 'id_port5')->dropDownList(
        ArrayHelper::map(Port::find()->all(), 'id_port', 'nama_port'),
        [
            'id' => "Otbports_{$key}_id_port5",
            'name' => "Otbports[$key][id_port5]",
            'class' => 'select2',
            'style' => 'width: 100%',
            'prompt' => 'Select Port',
        ])->label(false) ?>
</td>
<td>
    <?= $form->field($otbport, 'status')->textInput([
        'id' => "Otbports_{$key}_status",
        'name' => "Otbports[$key][status]",
        'disabled' => '',
    ])->label(false) ?>
</td>

<td>
    <?= Html::a('Remove', 'javascript:void(0);', [
        'class' => 'otb-remove-otbport-button btn btn-danger btn-xs',
    ]) ?>
</td>