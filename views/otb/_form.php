<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\OtbPort;

/* @var $this yii\web\View */
/* @var $model app\models\Otb */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false, // TODO get this working with client validation
    ]); ?>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model->otb, 'nama_otb')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-3">
            <?= $form->field($model->otb, 'alias_otb')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">OTB Port</h4>
                    <?php
                    // new otbport button
                    echo Html::a('New OTB Port', 'javascript:void(0);', [
                        'id' => 'otb-new-otbport-button',
                        'class' => 'pull-right btn btn-primary'
                    ])
                    ?>
                </div>
                <div class="box-body">
                    <?php

                    // otbport table
                    $otbport = new OtbPort();
                    $otbport->loadDefaultValues();
                    echo '<table id="otb-otbports" class="table table-condensed table-bordered">';
                    echo '<thead>';
                    echo '<tr>';
                    echo '<th>' . $otbport->getAttributeLabel('id_port1') . '</th>';
                    echo '<th>' . $otbport->getAttributeLabel('id_port2') . '</th>';
                    echo '<th>' . $otbport->getAttributeLabel('id_port3') . '</th>';
                    echo '<th>' . $otbport->getAttributeLabel('id_port4') . '</th>';
                    echo '<th>' . $otbport->getAttributeLabel('id_port5') . '</th>';
                    echo '<th>' . $otbport->getAttributeLabel('status') . '</th>';
                    echo '<td>&nbsp;</td>';
                    echo '</tr>';
                    echo '</thead>';
                    echo '<tbody>';

                    // existing otbports fields
                    foreach ($model->otbports as $key => $_otbport) {
                        echo '<tr>';
                        echo $this->render('_form-otb-otbport', [
                            'key' => $_otbport->isNewRecord ? (strpos($key, 'new') !== false ? $key : 'new' . $key) : $_otbport->id_oport,
                            'form' => $form,
                            'otbport' => $_otbport,
                        ]);
                        echo '</tr>';
                    }

                    // new otbport fields
                    echo '<tr id="otb-new-otbport-block" style="display: none;">';
                    echo $this->render('_form-otb-otbport', [
                        'key' => '__id__',
                        'form' => $form,
                        'otbport' => $otbport,
                    ]);
                    echo '</tr>';
                    echo '</tbody>';
                    echo '</table>';
                    ?>

                    <?php ob_start(); // output buffer the javascript to register later ?>
                    <script>
                        // add otbport button
                        var otbport_k = <?php echo isset($key) ? str_replace('new', '', $key) : 0; ?>;
                        $('#otb-new-otbport-button').on('click', function () {
                            otbport_k += 1;
                            $('#otb-otbports').find('tbody')
                                .append('<tr>' + $('#otb-new-otbport-block').html().replace(/__id__/g, 'new' + otbport_k) + '</tr>');

                            setSelect2();
                        });

                        // remove otbport button
                        $(document).on('click', '.otb-remove-otbport-button', function () {
                            $(this).closest('tbody tr').remove();
                        });

                        <?php
                        // OPTIONAL: click add when the form first loads to display the first new row
                        if (!Yii::$app->request->isPost && $model->otb->isNewRecord)
                            echo "$('#otb-new-otbport-button').click();";
                        ?>

                        //Re-Initialize Select2 Elements on each row
                        function setSelect2() {
                            if ($('.select2').data('select2') == undefined && $('.select2').next().hasClass('select2-container')) {
                                $('.select2').next().remove();
                            }

                            $('.select2').select2({
                                placeholder: 'Select Port',
                            });
                        }

                        $(document).ready(function () {
                            //Initialize Select2 Elements
                            $('.select2').select2({
                                placeholder: 'Select Port',
                            });
                        });
                    </script>
                    <?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 text-right">
            <?= Html::submitButton('<i class="fa fa-save"></i>Save', ['class' => 'btn btn-app bg-green']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
