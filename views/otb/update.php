<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Otb */

$this->title = 'Update Otb: ' . $model->otb->alias_otb;
$this->params['breadcrumbs'][] = ['label' => 'OTB', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->otb->alias_otb, 'url' => ['view', 'id' => $model->otb->id_otb]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-edit"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
