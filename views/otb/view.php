<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Otb */

$this->title = $model->alias_otb;
$this->params['breadcrumbs'][] = ['label' => 'OTB', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-search"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

        <p>
            <?= Html::a('<i class="fa fa-pencil"></i>Update', ['update', 'id' => $model->id_otb], ['class' => 'btn btn-app bg-blue']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>Delete', ['delete', 'id' => $model->id_otb], [
                'class' => 'btn btn-app bg-red-active',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [

                'nama_otb',
                'alias_otb',
            ],
        ]) ?>
        <br>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title">OTB Port</h4>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $port,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'id_port1',
                            'value' => 'port1.nama_port',
                        ],
                        [
                            'attribute' => 'id_port2',
                            'value' => 'port2.nama_port',
                        ],
                        [
                            'attribute' => 'id_port3',
                            'value' => 'port3.nama_port',
                        ],
                        [
                            'attribute' => 'id_port4',
                            'value' => 'port4.nama_port',
                        ],
                        [
                            'attribute' => 'id_port5',
                            'value' => 'port5.nama_port',
                        ],
                        'status',
                    ],
                ]) ?>
            </div>
        </div>

    </div>
</div>
