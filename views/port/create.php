<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Port */

$this->title = 'Create Port';
$this->params['breadcrumbs'][] = ['label' => 'Port', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-file-text"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
