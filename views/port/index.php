<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PortSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Port';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-list-alt"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

        <?php Pjax::begin(); ?>

        <p>
            <?= Html::a('<i class="fa fa-file-text"></i>Create', ['create'], ['class' => 'btn btn-app bg-red']) ?>
        </p>

        <?= DataTables::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'nama_port',
                'tipe_port',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
