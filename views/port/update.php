<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Port */

$this->title = 'Update Port: ' . $model->id_port;
$this->params['breadcrumbs'][] = ['label' => 'Port', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_port, 'url' => ['view', 'id' => $model->id_port]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-edit"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
