<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Ruas;
use app\models\MetroPort;
use app\models\Slot;
use app\models\DwdmPort;
use app\models\OtbPort;
use app\models\Kapasitas;
use app\models\Lambda;

/* @var $this yii\web\View */
/* @var $model app\models\Potensi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'id_ruas_near')->dropDownList(
                ArrayHelper::map(
                    Ruas::find()->asArray()->all(),
                    'id_ruas',
                    function ($model) {
                        return $model['nama_ruas'] . ' (' . $model['alias_ruas'] . ')';
                    }
                ),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'id_ruas_end')->dropDownList(
                ArrayHelper::map(
                    Ruas::find()->asArray()->all(),
                    'id_ruas',
                    function ($model) {
                        return $model['nama_ruas'] . ' (' . $model['alias_ruas'] . ')';
                    }
                ),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'id_mport_a')->dropDownList(
                ArrayHelper::map(MetroPort::find()->with(['metro', 'port1', 'port2', 'port3'])->asArray()->all(),
                    'id_mport',
                    function ($model) {
                        return $model['metro']['alias_metro'] . ' ' . $model['port1']['nama_port'] . '/' . $model['port2']['nama_port'] . '/' . $model['port3']['nama_port'];
                    }
                ),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'id_mport_b')->dropDownList(
                ArrayHelper::map(MetroPort::find()->with(['metro', 'port1', 'port2', 'port3'])->asArray()->all(),
                    'id_mport',
                    function ($model) {
                        return $model['metro']['alias_metro'] . ' ' . $model['port1']['nama_port'] . '/' . $model['port2']['nama_port'] . '/' . $model['port3']['nama_port'];
                    }
                ),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'id_slot_a')->dropDownList(
                ArrayHelper::map(Slot::find()->all(), 'id_slot', 'nama_slot'),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'id_slot_b')->dropDownList(
                ArrayHelper::map(Slot::find()->all(), 'id_slot', 'nama_slot'),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'id_dport_a')->dropDownList(
                ArrayHelper::map(DwdmPort::find()->with(['dwdm', 'port1', 'port2', 'port3'])->asArray()->all(),
                    'id_dport',
                    function ($model) {
                        return $model['dwdm']['alias_dwdm'] . ' ' . $model['port1']['nama_port'] . '/' . $model['port2']['nama_port'] . '/' . $model['port3']['nama_port'];
                    }
                ),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'id_dport_b')->dropDownList(
                ArrayHelper::map(DwdmPort::find()->with(['dwdm', 'port1', 'port2', 'port3'])->asArray()->all(),
                    'id_dport',
                    function ($model) {
                        return $model['dwdm']['alias_dwdm'] . ' ' . $model['port1']['nama_port'] . '/' . $model['port2']['nama_port'] . '/' . $model['port3']['nama_port'];
                    }
                ),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'id_oport_a')->dropDownList(
                ArrayHelper::map(OtbPort::find()->with(['otb', 'port1', 'port2', 'port3', 'port4', 'port5'])->asArray()->all(),
                    'id_oport',
                    function ($model) {
                        return $model['otb']['alias_otb'] . ' ' . $model['port1']['nama_port'] . '/' . $model['port2']['nama_port'] . '/' . $model['port3']['nama_port'] . '/' . $model['port4']['nama_port'] . '/' . $model['port5']['nama_port'];
                    }
                ),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
        <div class="col-xs-6">
            <?= $form->field($model, 'id_oport_b')->dropDownList(
                ArrayHelper::map(OtbPort::find()->with(['otb', 'port1', 'port2', 'port3', 'port4', 'port5'])->asArray()->all(),
                    'id_oport',
                    function ($model) {
                        return $model['otb']['alias_otb'] . ' ' . $model['port1']['nama_port'] . '/' . $model['port2']['nama_port'] . '/' . $model['port3']['nama_port'] . '/' . $model['port4']['nama_port'] . '/' . $model['port5']['nama_port'];
                    }
                ),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'id_kapasitas')->dropDownList(
                ArrayHelper::map(Kapasitas::find()->all(), 'id_kapasitas', 'jml_kapasitas'),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <?= $form->field($model, 'id_lambda')->dropDownList(
                ArrayHelper::map(Lambda::find()->all(), 'id_lambda', 'no_lambda'),
                [
                    'class' => 'select2',
                    'style' => 'width: 100%',
                    'prompt' => 'Please Select...',
                ]) ?>
        </div>
    </div>

    <?php ob_start(); ?>
    <script>
        $(document).ready(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
        });
    </script>
    <?php $this->registerJs(str_replace(['<script>', '</script>'], '', ob_get_clean())); ?>

    <div class="row">
        <div class="col-xs-12 text-right">
            <?= Html::submitButton('<i class="fa fa-save"></i>Save', ['class' => 'btn btn-app bg-green']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
