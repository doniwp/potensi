<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Potensi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-list-alt"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">

        <?php Pjax::begin(); ?>

        <p>
            <?= Html::a('<i class="fa fa-file-text"></i>Create', ['create'], ['class' => 'btn btn-app bg-red']) ?>
        </p>

        <?= DataTables::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'id_ruas_near',
                    'value' => function ($model) {
                        return $model->ruasNear->nama_ruas . ' - ' . $model->ruasNear->alias_ruas;
                    },
                ],
                [
                    'attribute' => 'id_ruas_end',
                    'value' => function ($model) {
                        return $model->ruasEnd->nama_ruas . ' - ' . $model->ruasEnd->alias_ruas;
                    },
                ],
                [
                    'attribute' => 'id_mport_a',
                    'value' => function ($model) {
                        return $model->mportA->metro->alias_metro . ' ' . $model->mportA->port1->nama_port . '/' . $model->mportA->port2->nama_port . '/' . $model->mportA->port3->nama_port;
                    },
                ],
                [
                    'attribute' => 'id_mport_b',
                    'value' => function ($model) {
                        return $model->mportB->metro->alias_metro . ' ' . $model->mportB->port1->nama_port . '/' . $model->mportB->port2->nama_port . '/' . $model->mportB->port3->nama_port;
                    },
                ],
                //'id_slot_a',
                //'id_slot_b',
                //'id_dport_a',
                //'id_dport_b',
                //'id_oport_a',
                //'id_oport_b',
                //'id_kapasitas',
                //'id_lambda',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>