<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Potensi */

$this->title = 'Update Potensi: ' . $model->ruasNear->alias_ruas . ' - ' . $model->ruasEnd->alias_ruas;
$this->params['breadcrumbs'][] = ['label' => 'Potensi', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ruasNear->alias_ruas . ' - ' . $model->ruasEnd->alias_ruas, 'url' => ['view', 'id' => $model->id_trans]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-edit"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
