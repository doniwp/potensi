<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Potensi */

$this->title = $model->ruasNear->alias_ruas . ' - ' . $model->ruasEnd->alias_ruas;
$this->params['breadcrumbs'][] = ['label' => 'Potensi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-search"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="box-body">


        <p>
            <?= Html::a('<i class="fa fa-pencil"></i>Update', ['update', 'id' => $model->id_trans], ['class' => 'btn btn-app bg-blue']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>Delete', ['delete', 'id' => $model->id_trans], [
                'class' => 'btn btn-app bg-red-active',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'id_ruas_near',
                    'value' => $model->ruasNear->nama_ruas . ' - ' . $model->ruasNear->alias_ruas,
                ],
                [
                    'attribute' => 'id_ruas_end',
                    'value' => $model->ruasEnd->nama_ruas . ' - ' . $model->ruasEnd->alias_ruas,
                ],
                [
                    'attribute' => 'id_mport_a',
                    'value' => $model->mportA->metro->alias_metro . ' ' . $model->mportA->port1->nama_port . '/' . $model->mportA->port2->nama_port . '/' . $model->mportA->port3->nama_port,
                ],
                [
                    'attribute' => 'id_mport_b',
                    'value' => $model->mportB->metro->alias_metro . ' ' . $model->mportB->port1->nama_port . '/' . $model->mportB->port2->nama_port . '/' . $model->mportB->port3->nama_port,
                ],
                [
                    'attribute' => 'id_slot_a',
                    'value' => $model->slotA->nama_slot,
                ],
                [
                    'attribute' => 'id_slot_b',
                    'value' => $model->slotB->nama_slot,
                ],
                [
                    'attribute' => 'id_dport_a',
                    'value' => $model->dportA->dwdm->alias_dwdm . ' ' . $model->dportA->port1->nama_port . '/' . $model->dportA->port2->nama_port . '/' . $model->dportA->port3->nama_port,
                ],
                [
                    'attribute' => 'id_dport_b',
                    'value' => $model->dportB->dwdm->alias_dwdm . ' ' . $model->dportB->port1->nama_port . '/' . $model->dportB->port2->nama_port . '/' . $model->dportB->port3->nama_port,
                ],
                [
                    'attribute' => 'id_oport_a',
                    'value' => $model->oportA->otb->alias_otb . ' ' . $model->oportA->port1->nama_port . '/' . $model->oportA->port2->nama_port . '/' . $model->oportA->port3->nama_port . '/' . $model->oportA->port4->nama_port . '/' . $model->oportA->port5->nama_port,
                ],
                [
                    'attribute' => 'id_oport_b',
                    'value' => $model->oportB->otb->alias_otb . ' ' . $model->oportB->port1->nama_port . '/' . $model->oportB->port2->nama_port . '/' . $model->oportB->port3->nama_port . '/' . $model->oportB->port4->nama_port . '/' . $model->oportB->port5->nama_port,
                ],
                [
                    'attribute' => 'id_kapasitas',
                    'value' => $model->kapasitas->jml_kapasitas,
                ],
                [
                    'attribute' => 'id_lambda',
                    'value' => $model->lambda->no_lambda,
                ],
            ],
        ]) ?>

    </div>
</div>
