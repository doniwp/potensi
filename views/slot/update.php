<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Slot */

$this->title = 'Update Slot: ' . $model->nama_slot;
$this->params['breadcrumbs'][] = ['label' => 'Slot', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_slot, 'url' => ['view', 'id' => $model->id_slot]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-edit"></i>
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
